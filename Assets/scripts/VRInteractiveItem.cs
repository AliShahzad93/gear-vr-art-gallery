using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace VRStandardAssets.Utils
{
    // This class should be added to any gameobject in the scene
    // that should react to input based on the user's gaze.
    // It contains events that can be subscribed to by classes that
    // need to know about input specifics to this gameobject.
    public class VRInteractiveItem : MonoBehaviour
    {
        public event Action OnOver;             // Called when the gaze moves over this object
        public event Action OnOut;              // Called when the gaze leaves this object
        public event Action OnClick;            // Called when click input is detected whilst the gaze is over this object.
        public event Action OnDoubleClick;      // Called when double click input is detected whilst the gaze is over this object.
        public event Action OnUp;               // Called when Fire1 is released whilst the gaze is over this object.
        public event Action OnDown;             // Called when Fire1 is pressed whilst the gaze is over this object.


        
        protected bool m_IsOver;


        public bool IsOver
        {
            get
            {
                return m_IsOver;
                }
            }


        // The below functions are called by the VREyeRaycaster when the appropriate input is detected.
        // They in turn call the appropriate events should they have subscribers.
        public void Over()
        {
            m_IsOver = true;

            if (OnOver != null)
                OnOver();
        }

       


        public void Out()
        {
            
            m_IsOver = false;

            if (OnOut != null)
                OnOut();
             StartCoroutine(hideInfo());
            
        }

        Text myText;
        Image myPanel;

        public void Click()
        {
            if (OnClick != null)
                OnClick();
            
            showInfo();
            Debug.Log("Clicked"+this.name);
        }



        IEnumerator hideInfo()
        {
            
            myText = GameObject.Find("objectInfo").GetComponent<Text>();
            myPanel = GameObject.Find("Panel").GetComponent<Image>();
            if (myText.text == "Tap on a Painting to know about it.")
            {
                yield return new WaitForSeconds(5);
              
            }
            myText.text = " ";
            myPanel.enabled = false;

        }

       public void showInfo()
        {

            myText = GameObject.Find("objectInfo").GetComponent<Text>();
            myPanel = GameObject.Find("Panel").GetComponent<Image>();
            myPanel.enabled = true;

            if (this.name == "Painting1")
                myText.text = "American Gothic, 1930. By: Grant Wood.";

            if (this.name == "Painting2")
                myText.text = "Whistler's Mother, 1871. By: James McNeill Whistler.";

            if (this.name == "Painting3")
                myText.text = "The Scream, 1893. By: Edvard Munch.";

            if (this.name == "Painting4")
                myText.text = "Mona Lisa, c. 1503-1519. By: Leonardo da Vinci.";

            if (this.name == "Painting5")
                myText.text = "The Starry Night, 1889. By: Vincent van Gogh.";
          
        }
        public void DoubleClick()
        {
            if (OnDoubleClick != null)
                OnDoubleClick();
           
        }


        public void Up()
        {
            if (OnUp != null)
                OnUp();
          
        }


        public void Down()
        {
            if (OnDown != null)
                OnDown();
        }
    }
}